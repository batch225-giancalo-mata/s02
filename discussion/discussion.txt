Gian Carlo Mata


Basic Git Commands

ssh-keygen
- is used to generate ssh keys

git config --global user.email "[git account email address]"
- used to configure the global user email

git config --global user.name "[git account username]"
- used to configure the global user name

git config --global --list
- used to check the git user credentials

git init
- triggering this command will create a ".git" folder inside of a project folder
- this folder will contain all the necessary git information about a project's local repository such as commits and remote git links

git status
- triggring this command will display all updates not yet saved to the latest commit version of the project
- a "commit" is a snapshot or a version of the project

git add .
- stages files individually
- trigerring this command will "add" or "stage" the files preparing them to be included in the next commit/snapshot of the project
- adding all files is normal practice to make sure all files that were updated would be included in the next commit. Individually staging files is useful for minor revisions.

git commit -m "message"
- trigerring this command will create a commit/snapshot of the project in the local repository coming from changes added through
- "Initial commit" is used in the message to help developers identify the first commit
- Succeeding commit messages should be descriptive of the changes of the project (ex. "added text to discussion.txt file")

git log/git log --oneline
- check the commit history

git remote add [remote-name] [git-repository-link]
- trigerring this command will add a reference to a git project 
- "origin" is mostly used to help developers identify the main remote repository linked to the project
- multiple remote repositories can be added to a project. This would be useful for students when dealing with cloned repositories or for creating multiple repositories for their capstone project.
	example:
	git remote add origin git@gitlab.com:batch225-giancalo-mata/s02.git
	(clone with SSH)

git remote -v
- check the remote names and their corresponding urls.

git remote remove [remote-name]
- remove a remote repository
	example:
	git remote remove origin

git push [remote-name] [branch-name]
- upload the local repository to a remote repository
	example:
	git push origin master
	git push origin main (mac)

git clone [git-repository-link]
- clones a repository
	example:
	git clone git@gitlab.com:batch225-giancalo-mata/s02.git


Usual everyday routine

1. git init (inside of a session folder, e.g. inside s02)
2. git add . (to stage changes)
3. git commit -m "[commit message]"
4. git remote add origin [link]
5. git push origin master